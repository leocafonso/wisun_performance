import click
from WiSUN import WiSUN
import time
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

__author__ = "Leo"

@click.group()
def main():
    """
    Teste de desempenho WiSUN FAN v1.
    """
    pass

@main.command()
@click.argument('com', nargs=1)
@click.argument('xml', nargs=1)
def load(com, xml):
    """Irá carregar o xml de configuracão na placa na porta COM"""
    node = WiSUN(com)
    node.reset()
    node.load(xml)

@main.command()
@click.argument('com')
def start(com):
    """Inicializa o processo de Joining"""
    node = WiSUN(com)
    node.start()
    status = node.status()
    print(status)
    while (status[0] != '05'):
        status = node.status()
        print(status)
        time.sleep(2)
    print('Conectado')

@main.command()
@click.argument('com')
def status(com):
    """Status do processo de Joining"""
    node = WiSUN(com)
    status = node.status()
    print(status)

@main.command()
@click.argument('com')
def routeList(com):
    """Status do processo de Joining"""
    node = WiSUN(com)
    status = node.route_list()
    print(status)

@main.command()
@click.argument('com', nargs=1)
@click.argument('addr1', nargs=1)
@click.argument('addr2', nargs=1)
def whitelist_set(com, addr1, addr2):
    """Whitelist set"""
    node = WiSUN(com)
    print(node.whitelist_set(addr1, addr2))

@main.command()
@click.argument('com', nargs=1)
def whitelist_get(com):
    """Whitelist get"""
    node = WiSUN(com)
    print(node.whitelist_get())


@main.command()
@click.argument('com', nargs=1)
@click.argument('src', nargs=1)
@click.argument('dst', nargs=1)
@click.argument('port', nargs=1)
@click.argument('payload', nargs=1)
def udpsend(com, src, dst, port, payload):
    """Envia dados via UDP"""
    node = WiSUN(com)
    payloadSize = len(payload.encode('utf-8'))
    print('Tamanho do pacote: {}'.format(payloadSize))
    print(node.udp_send(dst,src,port,port,0,payload,payloadSize,0))

    
if __name__ == "__main__":
    main()