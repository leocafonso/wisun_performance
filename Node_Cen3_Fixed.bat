set CEN=Cen3
set CHAN=Fixed

set COM0=COM20
set COM1=COM15
set COM2=COM18
set COM3=COM16
set COM4=COM31
set COM5=COM32
set COM6=COM33
set XMLBR0=Config\%CEN%\%CHAN%\BorderRouter_US_Band_50kbps_%CHAN%_0.xml
set XML1=Config\%CEN%\%CHAN%\RouterNode_US_Band_50kbps_%CHAN%_1.xml
set XML2=Config\%CEN%\%CHAN%\RouterNode_US_Band_50kbps_%CHAN%_2.xml
set XML3=Config\%CEN%\%CHAN%\RouterNode_US_Band_50kbps_%CHAN%_3.xml
set XML4=Config\%CEN%\%CHAN%\RouterNode_US_Band_50kbps_%CHAN%_4.xml
set XML5=Config\%CEN%\%CHAN%\RouterNode_US_Band_50kbps_%CHAN%_5.xml
set XML6=Config\%CEN%\%CHAN%\RouterNode_US_Band_50kbps_%CHAN%_6.xml


python .\testCLI.py load %COM0% %XMLBR0%
python .\testCLI.py start %COM0%

python .\testCLI.py load %COM1% %XML1%
python .\testCLI.py start %COM1%

python .\testCLI.py load %COM2% %XML2%
python .\testCLI.py start %COM2%

python .\testCLI.py load %COM3% %XML3% 
python .\testCLI.py start %COM3%

python .\testCLI.py load %COM4% %XML4%
python .\testCLI.py start %COM4%

python .\testCLI.py load %COM5% %XML5%
python .\testCLI.py start %COM5%

python .\testCLI.py load %COM6% %XML6%
python .\testCLI.py start %COM6%
