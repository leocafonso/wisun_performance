import click
from WiSUN import WiSUN
import time
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt

__author__ = "Leo"

@click.group()
def main():
    """
    Teste de desempenho WiSUN FAN v1.
    """
    pass

@main.command()
@click.argument('com', nargs=1)
@click.argument('xml', nargs=1)
def load(com, xml):
    """Irá carregar o xml de configuracão na placa na porta COM"""
    node = WiSUN(com)
    node.reset()
    node.load(xml)

@main.command()
@click.argument('com')
def start(com):
    """Inicializa o processo de Joining"""
    node = WiSUN(com)
    node.start()
    status = node.status()
    print(status)
    # while (status[0] != '05'):
    #     status = node.status()
    #     print(status)
    #     time.sleep(2)
    # print('Conectado')

@main.command()
@click.argument('com')
def status(com):
    """Status do processo de Joining"""
    node = WiSUN(com)
    status = node.status()
    print(status)

@main.command()
@click.argument('com')
def routeList(com):
    """Status do processo de Joining"""
    node = WiSUN(com)
    status = node.route_list()
    print(status)

@main.command()
@click.argument('com', nargs=1)
@click.argument('addr1', nargs=1)
@click.argument('addr2', nargs=1)
def whitelist_set(com, addr1, addr2):
    """Whitelist set"""
    node = WiSUN(com)
    print(node.whitelist_set(addr1, addr2))

@main.command()
@click.argument('com', nargs=1)
def whitelist_get(com):
    """Whitelist get"""
    node = WiSUN(com)
    print(node.whitelist_get())

@main.command()
@click.argument('com', nargs=1)
@click.argument('src', nargs=1)
@click.argument('dst', nargs=1)
@click.argument('payload', nargs=1)
def pingPerf(com, src, dst, payload):
    """Desempenho do ping"""
    timeElapsed = []
    payloadSize = len(payload.encode('utf-8'))
    node = WiSUN(com)
    for _ in range(30):
        node.icmp_ping_req(src, dst, payload, payloadSize/2, 0)
        tb = time.time()
        node.icmp_ping_reply()
        ta = time.time()
        print (ta - tb)
        timeElapsed.append(ta - tb)
        time.sleep(0.1)
    print('Average: {}'.format(np.average(timeElapsed)))
    print('SD: {}'.format(np.std(timeElapsed)))
    plt.plot(timeElapsed)
    plt.show()

@main.command()
@click.argument('com', nargs=1)
@click.argument('src1', nargs=1)
@click.argument('src2', nargs=1)
@click.argument('dst', nargs=1)
@click.argument('payload', nargs=1)
def pingPerf2(com, src1, src2, dst, payload):
    """Desempenho do ping"""
    timeElapsed1 = []
    timeElapsed2 = []
    timeElapsed3 = []

    medias = []
    desvios = []
    x= range(30)
    payloadSize = len(payload.encode('utf-8'))
    node = WiSUN(com)
    print ('Ping do no {} para o {}'.format(src1, dst))
    for i in x:
        node.icmp_ping_req(src1, dst, payload, payloadSize/2, 0)
        tb = time.time()
        node.icmp_ping_reply()
        ta = time.time()
        print (ta - tb)
        timeElapsed1.append(ta - tb)
        time.sleep(0.1)
    print('Average: {}'.format(np.average(timeElapsed1)))
    medias.append(np.average(timeElapsed1))
    print('SD: {}'.format(np.std(timeElapsed1)))
    desvios.append(np.std(timeElapsed1))
    timeElapsed1.clear
    print ('Ping do no {} para o {}'.format(src2, dst))
    for _ in x:
        node.icmp_ping_req(src2, dst, payload, payloadSize/2, 0)
        tb = time.time()
        node.icmp_ping_reply()
        ta = time.time()
        print (ta - tb)
        timeElapsed2.append(ta - tb)
        time.sleep(0.1)
    print('Average: {}'.format(np.average(timeElapsed2)))
    medias.append(np.average(timeElapsed2))
    print('SD: {}'.format(np.std(timeElapsed2)))
    desvios.append(np.std(timeElapsed2))
    plt.errorbar(range(len(medias)), medias, yerr = desvios, label='Latencia')
    plt.xlabel('Número de saltos')
    plt.ylabel('Latência (ms)')
    plt.show()

@main.command()
@click.argument('com1', nargs=1)
@click.argument('com2', nargs=1)
@click.argument('com3', nargs=1)
@click.argument('src1', nargs=1)
@click.argument('src2', nargs=1)
@click.argument('src3', nargs=1)
@click.argument('dst', nargs=1)
@click.argument('payload', nargs=1)
def pingPerf3(com1, com2, com3, src1, src2, src3, dst, payload):
    """Desempenho do ping"""
    timeElapsed1 = []
    timeElapsed2 = []
    timeElapsed3 = []
    medias = []
    desvios = []
    x= range(30)
    payloadSize = len(payload.encode('utf-8'))
    node1 = WiSUN(com1)  
    print ('Ping do no {} para o {}'.format(src1, dst))
    for i in x:
        node1.icmp_ping_req(dst,src1,payload, payloadSize/2, 0)
        tb = time.time()
        node1.icmp_ping_reply()
        ta = time.time()
        if (timeElapsed1 != []):
            while(((ta - tb) - timeElapsed1[i - 1]) > 0.2):
                node1.icmp_ping_req(src1, dst, payload, payloadSize/2, 0)
                tb = time.time()
                node1.icmp_ping_reply()
                ta = time.time()
        print (ta - tb)
        timeElapsed1.append(ta - tb)
        #time.sleep(0.1)
    print('Average: {}'.format(np.average(timeElapsed1)))
    medias.append(np.average(timeElapsed1))
    print('SD: {}'.format(np.std(timeElapsed1)))
    desvios.append(np.std(timeElapsed1))
    del node1

    node2 = WiSUN(com2) 
    print ('Ping do no {} para o {}'.format(src2, dst))
    for i in x:
        node2.icmp_ping_req(dst,src2,payload, payloadSize/2, 0)
        tb = time.time()
        node2.icmp_ping_reply()
        ta = time.time()
        if (timeElapsed2 != []):
            while(((ta - tb) - timeElapsed1[i - 1]) > 0.2):
                node2.icmp_ping_req(src1, dst, payload, payloadSize/2, 0)
                tb = time.time()
                node2.icmp_ping_reply()
                ta = time.time()        
        print (ta - tb)
        timeElapsed2.append(ta - tb)
        #time.sleep(0.5)
    print('Average: {}'.format(np.average(timeElapsed2)))
    medias.append(np.average(timeElapsed2))
    print('SD: {}'.format(np.std(timeElapsed2)))
    desvios.append(np.std(timeElapsed2))
    del node2

    node3 = WiSUN(com3) 
    print ('Ping do no {} para o {}'.format(src3, dst))
    for _ in x:
        node3.icmp_ping_req(dst,src3,payload, payloadSize/2, 0)
        tb = time.time()
        node3.icmp_ping_reply()
        ta = time.time()
        if (timeElapsed3 != []):
            while(((ta - tb) - timeElapsed1[i - 1]) > 0.2):
                node3.icmp_ping_req(src1, dst, payload, payloadSize/2, 0)
                tb = time.time()
                node3.icmp_ping_reply()
                ta = time.time()
        print (ta - tb)
        timeElapsed3.append(ta - tb)
        #time.sleep(0.5)
    print('Average: {}'.format(np.average(timeElapsed3)))
    medias.append(np.average(timeElapsed3))
    print('SD: {}'.format(np.std(timeElapsed3)))
    desvios.append(np.std(timeElapsed3))
    del node3

    plt.xlabel('Número de ping')
    plt.ylabel('Latência (ms)')
    plt.xticks(np.arange(0,30,1))
    plt.grid()
    plt.plot(x, timeElapsed1, marker='o')
    plt.plot(x, timeElapsed2, marker='^')
    plt.plot(x, timeElapsed3, marker='+')
    plt.legend(('R1 -> BR0', 'R2 -> BR0', 'R3 -> BR0'),
           loc='lower right')
    plt.show()

    plt.errorbar(range(len(medias)), medias, yerr = desvios, label='Latencia', fmt='o',ecolor='g', capthick=2)
    plt.xlabel('Número de saltos')
    plt.ylabel('Latência (ms)')
    plt.xticks(np.arange(0,3,1))
    plt.grid()
    plt.show()

@main.command()
@click.argument('com', nargs=1)
@click.argument('src', nargs=1)
@click.argument('dst', nargs=1)
@click.argument('port', nargs=1)
#@click.argument('payload', nargs=1)
def udpPerf(com, src, dst, port):
    node = WiSUN(com)
    payload = '0'
#    for i in range(971):
    for i in range(971):
       payload = payload + '0'
    payloadSize = len(payload.encode('utf-8'))
    print('Tamanho do pacote: {}'.format(payloadSize))
    tb = time.time()  
    # for i in range(30):
    #     node.udp_send(dst,src,port,port,0,payload,payloadSize,0)
    print(node.udp_send(dst,src,port,port,0,payload,payloadSize,0))
    ta = time.time()
    print (ta - tb)
    #print ((ta - tb)/30)

@main.command()
@click.argument('com', nargs=1)
@click.argument('src', nargs=1)
@click.argument('dst', nargs=1)
@click.argument('port', nargs=1)
@click.argument('payload', nargs=1)
def udpsend(com, src, dst, port, payload):
    node = WiSUN(com)
    payloadSize = len(payload.encode('utf-8'))
    print('Tamanho do pacote: {}'.format(payloadSize))
    print(node.udp_send(dst,src,port,port,0,payload,payloadSize,0))



@main.command()
def teste():
    """Teste do grafico"""
    x = np.array([0, 1, 2]).reshape((-1, 1))
    y = np.array([0.03,0.110, 0.150])
    saltos = np.arange(0,25,1)
    model = LinearRegression().fit(x, y)
    r_sq = model.score(x, y)
    print('coefficient of determination:', r_sq)
    print('intercept:', model.intercept_)
    print('slope:', model.coef_)
    desvios = [0.01, 0.065, 0.06]
    plt.errorbar(np.arange(0,3,1), y, yerr = desvios, fmt='o',ecolor='g', capthick=2)
    est = model.coef_*saltos + model.intercept_
    plt.plot(saltos,est,color='red',   
         linewidth=1.0,  
         linestyle='--')
    plt.xlabel('Número de saltos')
    plt.ylabel('Latência (ms)')
    plt.xticks(saltos)
    plt.grid()
    # plt.legend(('No mask', 'Masked if > 0.5', 'Masked if < -0.5'),
    #        loc='lower right')
    plt.show()

@main.command()
def latencia_teoria():
    """Teste do grafico"""
    saltos = np.arange(0,25,1)
    melhor = 1.16*saltos
    pior = 97.44*saltos
    plt.plot(saltos,pior,color='red',   
         linewidth=1.0,  
         linestyle='--')
    plt.plot(saltos,melhor,color='green',   
         linewidth=1.0,  
         linestyle='--')
    plt.fill_between(saltos,melhor, pior, color='gray')
    plt.xlabel('Número de saltos')
    plt.ylabel('Latência (ms)')
    plt.xticks(saltos)
    plt.grid()
    plt.legend(('Pior caso', 'Melhor caso'),
           loc='Topper left')
    plt.show()
    
if __name__ == "__main__":
    main()