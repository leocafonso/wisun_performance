import pandas as pd
import glob
import os
import matplotlib.pyplot as plt
import numpy as np

all_files = glob.glob('./Resultados/Cen1/data*.csv')

li = []
value = []
header_list = ['node','payload','period','mode','ber','Average', 'Std']
test_payload = ['10','100','1000']
test_period = ['500', '1000','2000','5000']
test_mode = ['0','1']
for filename in all_files:
    fname = os.path.splitext(os.path.basename(filename))[0]
    df = pd.read_csv(filename, header=None)
    mean = np.mean(df.values)
    std = np.std(df.values)
    ber = len(df.index)
    node = int(fname.split('_')[1])
    payload = int(fname.split('_')[2])
    period = int(fname.split('_')[3])
    mode = int(fname.split('_')[4])
    li.append([node,payload,period,mode,ber,mean,std])
frame = pd.DataFrame(li)
frame.columns = header_list
ind = np.arange(4) 

for j in range(len(test_payload)):
    width = 0.1  
    desl = 0
    for i in range(6):
        avg = frame.query('node == {} and payload == {} and mode == 1'.format(i,test_payload[j]))
        avg.sort_values(by=['period'], inplace=True)
        plt.bar(ind + desl, avg['Average'].to_numpy(), width, label=str(avg['node'].values))
        desl +=  width
    plt.xticks(ind + desl / 2, test_period)
    plt.show()

