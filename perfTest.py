from WiSUN import WiSUN
import time
import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
import binascii
import struct
import array
import random
import pandas as pd

def main():
    """
    Teste de desempenho WiSUN FAN v1.
    """
    DST= [
        '20170000000000007690500000000001',
        '20170000000000007690500000000002',
        '20170000000000007690500000000003',
        '20170000000000007690500000000004',
        '20170000000000007690500000000005',
        '20170000000000007690500000000006',
    ]
    SRC = '20170000000000007690500000000000'
    PORT = '0E1A'
    PAYLOAD = '010203040506070809'
    COM_BR = 'COM20'
    XML_BR = '..\BorderRouter_US_Band_50kbps_Ch0.xml'
    node = WiSUN(COM_BR)
    # print("Border Router COM:", COM_BR)
    # value = input('Inicio: frio ou quente:')
    # if value == 'frio':
    #     print('Border Router xml:', XML_BR)
    #     load(node, XML_BR)
    #     start(node)
    # payloadSize = len(PAYLOAD.encode('utf-8')) 
    # ret = node.icmp_ping_req(DST,SRC, PAYLOAD, payloadSize/2, 0) 
    # while ret != 0:
    #     ret = node.icmp_ping_req(DST,SRC, PAYLOAD, payloadSize/2, 0)
    #     print(ret)
    #     print(node.icmp_ping_reply())
    #     time.sleep(1)
    value = input('ping/rotas/teste:')
    while value != 'exit':
        command = value.split()
        if command[0] == 'ping':
            node_num = command[1]
            payloadSize = len(PAYLOAD.encode('utf-8')) 
            if node_num == 'all':
                for node_ip in DST:
                    ret = node.icmp_ping_req(node_ip,SRC, PAYLOAD, payloadSize/2, 0)
                    print(ret)
                    time.sleep(1)
                print(node.icmp_ping_reply())
            if node_num.isnumeric():
                tb = time.time()
                ret = node.icmp_ping_req(DST[int(node_num) - 1],SRC, PAYLOAD, payloadSize/2, 0)
                print(ret)
                print(node.icmp_ping_reply())
                ta = time.time()
                print('RTT: {}'.format(ta-tb))
                time.sleep(1)

        if command[0] == 'teste':
            node_num = command[1]
            test_type = command[2]
            test_com = command[3]
            if (test_com == 'start'):
                test_payload = command[4]
                test_period = command[5]
                test_mode = command[6]
                udp_payload =  "{} {} {} {} {}".format(test_type, test_com, test_payload,test_period, test_mode)
            if (test_com == 'stop'):
                udp_payload =  "{} {}".format(test_type, test_com)            
            print(udp_payload)
            l1=[c for c in udp_payload]   # in Python, a string is just a sequence, so we can iterate over it!
            l2=[int(ord(c)) for c in udp_payload]
            texto = binascii.hexlify(bytearray(l2))
            print(texto.decode('utf-8'))
            print(l1)
            print(l2)
            payloadSize = len(texto.decode('utf-8')) 
            print(node.udp_send(DST[int(node_num) - 1],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0))

        if command[0] == 'teste_ber':
            test_type = command[1]
            test_com = command[2]
            if (test_com == 'start'):
                test_payload = command[3]
                test_period = command[4]
                test_mode = command[5]
                udp_payload =  "{} {} {} {} {}".format(test_type, test_com, test_payload,test_period, test_mode)
                #offset = (int(test_period)+int(test_period)/5)/1000
            if (test_com == 'stop'):
                udp_payload =  "{} {}".format(test_type, test_com) 
                offset = 1           
            print(udp_payload)
            l1=[c for c in udp_payload]   # in Python, a string is just a sequence, so we can iterate over it!
            l2=[int(ord(c)) for c in udp_payload]
            texto = binascii.hexlify(bytearray(l2))
            print(texto.decode('utf-8'))
            print(l1)
            print(l2)
            payloadSize = len(texto.decode('utf-8'))
            for i in range(6): 
                print(node.udp_send(DST[i],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0))
                offset = 0.05*random.randint(1,100)
                print(offset)
                time.sleep(offset)
    
        if command[0] == 'teste_init':
            test_type = command[1]
            test_com = command[2]
            if (test_com == 'start'):
                test_payload = ['10','100','1000']
                test_period = ['500', '1000','3000','5000']
                #test_mode = ['0','1']
                test_mode = ['0']
                timeList = []
                for k in range(len(test_mode)):
                    for j in range(len(test_period)):
                        for i in range(len(test_payload)):
                            if test_payload[i] == '100' and int(test_period[j]) < 1000:
                                break
                            if test_payload[i] == '1000' and int(test_period[j]) < 3000:
                                break
                            udp_payload =  "{} {} {} {} {}".format(test_type, test_com, test_payload[i],test_period[j], test_mode[k])
                            print(udp_payload)
                            l1=[c for c in udp_payload]   # in Python, a string is just a sequence, so we can iterate over it!
                            l2=[int(ord(c)) for c in udp_payload]
                            texto = binascii.hexlify(bytearray(l2))
                            print(texto.decode('utf-8'))
                            print(l1)
                            print(l2)
                            payloadSize = len(texto.decode('utf-8'))
                            for n in range(6): 
                                udp_res = node.udp_send(DST[n],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0)
                                print(udp_res)
                                while (len(udp_res) < 3):
                                    time.sleep(3)
                                    udp_res = node.udp_send(DST[n],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0)
                                    print(udp_res)
                                while (udp_res[2] != '00'):    
                                    time.sleep(3)
                                    udp_res = node.udp_send(DST[n],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0)
                                    print(udp_res)                                
                                offset = 0.05*random.randint(1,100)
                                print(offset)
                                time.sleep(offset)
                            if (int(test_payload[i]) == 1000 and int(test_period[j]) < 2000):
                                espera = 300
                            else:
                                espera = 60 + int(test_period[j])*100/1000    
                            print("Espera {} min".format(espera/60))
                            time.sleep(espera)
                            for n in range(6): 
                                node_num = str(n)
                                udp_payload =  "lat {}".format(n+1)            
                                print(udp_payload)
                                l1=[c for c in udp_payload]   # in Python, a string is just a sequence, so we can iterate over it!
                                l2=[int(ord(c)) for c in udp_payload]
                                texto = binascii.hexlify(bytearray(l2))
                                print(texto.decode('utf-8'))
                                print(l1)
                                print(l2)
                                payloadSize = len(texto.decode('utf-8'))
                                reply = []
                                while (len(reply) < 9):
                                    print(node.udp_send(DST[n],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0))
                                    reply = node.udp_rcvi()
                                    print(reply)
                                    print("Tamanho reply {}".format(len(reply)))
                                    time.sleep(2)
                                d = reply[9]
                                s = "".join([m[2:4]+m[0:2] for m in [d[i:i+4] for i in range(0,len(d),4)]])
                                timetable = [int(s[i:i+4],16) for i in range(0,len(s),4)]
                                while 0 in timetable: timetable.remove(0)
                                print(timetable)
                                timeList.append(timetable)
                                filename = "data_{}_{}_{}_{}.csv".format(node_num, test_payload[i],test_period[j], test_mode[k])
                                #pd.DataFrame
                                np.savetxt(filename, timetable, delimiter=',')
                                print(len(timetable))
                                x = np.arange(0, len(timetable), 1)
                                print(np.average(timetable))
                                print(np.std(timetable))
                                time.sleep(2)
            # filename = "data.csv"
            # np.savetxt(filename, timeList, delimiter=',')
            print(timeList)
            

            if (test_com == 'stop'):
                udp_payload =  "{} {}".format(test_type, test_com) 
                offset = 1           
           

        if command[0] == 'ber':
            node_num = command[1]
            udp_payload =  "{} {}".format(command[0] , command[1])            
            print(udp_payload)
            l1=[c for c in udp_payload]   # in Python, a string is just a sequence, so we can iterate over it!
            l2=[int(ord(c)) for c in udp_payload]
            texto = binascii.hexlify(bytearray(l2))
            print(texto.decode('utf-8'))
            print(l1)
            print(l2)
            payloadSize = len(texto.decode('utf-8')) 
            print(node.udp_send(DST[int(node_num) - 1],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0))
            reply = node.udp_rcvi()
            print(reply)
            d = reply[9]
            s = "".join([m[2:4]+m[0:2] for m in [d[i:i+4] for i in range(0,len(d),4)]])
            ber = int(s, 16)/100
            print(ber)

        if command[0] == 'lat':
            node_num = command[1]
            udp_payload =  "{} {}".format(command[0] , command[1])            
            print(udp_payload)
            l1=[c for c in udp_payload]   # in Python, a string is just a sequence, so we can iterate over it!
            l2=[int(ord(c)) for c in udp_payload]
            texto = binascii.hexlify(bytearray(l2))
            print(texto.decode('utf-8'))
            print(l1)
            print(l2)
            payloadSize = len(texto.decode('utf-8')) 
            print(node.udp_send(DST[int(node_num) - 1],SRC,PORT, PORT, 0, texto.decode('utf-8'), payloadSize, 0))
            reply = node.udp_rcvi()
            d = reply[9]
            s = "".join([m[2:4]+m[0:2] for m in [d[i:i+4] for i in range(0,len(d),4)]])
            #chunks, chunk_size = len(s), 4
            #timehexarray = [ s[i:i+chunk_size] for i in range(0, chunks, chunk_size) ]
            #print(int(timehexarray, 16))
            timetable = [int(s[i:i+4],16) for i in range(0,len(s),4)]
            while 0 in timetable: timetable.remove(0)
            print(timetable)
            filename = "data_{}.csv".format(node_num)
            np.savetxt(filename, timetable, delimiter=',')
            # plt.xlabel('Número de ping')
            # plt.ylabel('Latência (ms)')
            #plt.xticks(np.arange(0,30,1))
            print(len(timetable))
            x = np.arange(0, len(timetable), 1)
            print(np.average(timetable))
            print(np.std(timetable))
            # plt.grid()
            # plt.plot(x, timetable)
            # plt.show()
            

        if command[0] == 'rotas':  
            print(node.route_list())
        if command[0] == 'nbcache':  
            print(node.neighbor_cache())

        value = input('ping/rotas/teste:')
    del node
    print('fim')
    

def start(node):
    """Inicializa o processo de Joining"""
    node.start()
    status = node.status()
    print(status)
    while (status[0] != '05'):
        status = node.status()
        print(status)
        time.sleep(2)
    print('Conectado')

def load(node, xml):
    """Irá carregar o xml de configuracão na placa na porta COM"""
    node.reset()
    node.load(xml)

if __name__ == "__main__":
    main()