import pandas as pd
import glob
import os
import matplotlib.pyplot as plt
import seaborn as sn
import numpy as np



def main():
    all_files = glob.glob('./Resultados/Cen2/data*.csv')

    li = []
    value = []
    header_list = ['node','payload','period','mode','ber','Average', 'Std']
    test_payload = ['10','100','1000']
    test_period = ['500', '1000','2000','5000']
    test_mode = ['0','1']
    for filename in all_files:
        fname = os.path.splitext(os.path.basename(filename))[0]
        df = pd.read_csv(filename, header=None)
        mean = np.mean(df.values)
        std = np.std(df.values)
        ber = len(df.index)
        node = int(fname.split('_')[1])
        payload = int(fname.split('_')[2])
        period = int(fname.split('_')[3])
        mode = int(fname.split('_')[4])
        li.append([node,payload,period,mode,ber,mean,std])
    frame = pd.DataFrame(li)
    frame.columns = header_list
    #corrMatrix = find_correlation(frame)
    corrMatrix = frame.corr()
    print(corrMatrix)

    # sn.heatmap(corrMatrix, annot=True)
    # plt.show()

    # ind = np.arange(4) 
    # for j in range(len(test_payload)):
    #     width = 0.1  
    #     desl = 0
    #     for i in range(6):
    #         avg = frame.query('node == {} and payload == {} and mode == 0'.format(i,test_payload[j]))
    #         avg.sort_values(by=['period'], inplace=True)
    #         print(avg['ber'].to_numpy())
    #         x_offset = len(test_period) - len(avg['ber'].to_numpy())
    #         result = np.zeros(len(test_period))  
    #         result[x_offset:avg['ber'].to_numpy().shape[0]+x_offset] = avg['ber'].to_numpy()
    #         plt.bar(ind + desl, result, width, label=str(avg['node'].values))
    #         desl +=  width
    #     plt.xticks(ind + desl / 2, test_period)

    #     plt.show()

def find_correlation(data, threshold=0.9, remove_negative=False):
    """
    Given a numeric pd.DataFrame, this will find highly correlated features,
    and return a list of features to remove.
    Parameters
    -----------
    data : pandas DataFrame
        DataFrame
    threshold : float
        correlation threshold, will remove one of pairs of features with a
        correlation greater than this value.
    remove_negative: Boolean
        If true then features which are highly negatively correlated will
        also be returned for removal.
    Returns
    --------
    select_flat : list
        listof column names to be removed
    """
    corr_mat = data.corr()
    if remove_negative:
        corr_mat = np.abs(corr_mat)
    corr_mat.loc[:, :] = np.tril(corr_mat, k=-1)
    already_in = set()
    result = []
    for col in corr_mat:
        perfect_corr = corr_mat[col][corr_mat[col] > threshold].index.tolist()
        if perfect_corr and col not in already_in:
            already_in.update(set(perfect_corr))
            perfect_corr.append(col)
            result.append(perfect_corr)
    select_nested = [f[1:] for f in result]
    select_flat = [i for j in select_nested for i in j]
    return select_flat

if __name__ == "__main__":
    main()