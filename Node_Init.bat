set COM0=COM20
set COM1=COM15
set COM2=COM18
set COM3=COM16
set COM4=COM31
set COM5=COM32
set COM6=COM33
set XMLBR=..\BorderRouter_US_Band_50kbps_FH.xml
set XML=..\RouterNode_US_Band_50kbps_FH.xml
set NODE0=7490500000000000
set NODE1=7490500000000001
set NODE2=7490500000000002
set NODE3=7490500000000003
set NODE4=7490500000000004
set NODE5=7490500000000005
set NODE6=7490500000000006
set DUMP=0000000000000000

REM python .\testCLI.py load %COM0% %XMLBR%
REM REM python .\testCLI.py whitelist-set %COM0% %NODE2% %NODE3% %NODE4% %NODE5% %NODE6%
REM python .\testCLI.py start %COM0%

python .\testCLI.py load %COM1% %XML%
python .\testCLI.py whitelist-set %COM1% %NODE0% %DUMP%
python .\testCLI.py start %COM1%

REM python .\testCLI.py load %COM2% %XML%
REM python .\testCLI.py whitelist-set %COM2% %NODE0% %DUMP%
REM python .\testCLI.py start %COM2%

REM python .\testCLI.py load %COM3% %XML% 
REM python .\testCLI.py whitelist-set %COM3% %NODE0% %DUMP%
REM python .\testCLI.py start %COM3%

REM python .\testCLI.py load %COM4% %XML%
REM python .\testCLI.py whitelist-set %COM4% %NODE0% %DUMP%
REM python .\testCLI.py start %COM4%

REM python .\testCLI.py load %COM5% %XML%
REM python .\testCLI.py whitelist-set %COM5% %NODE0% %DUMP%
REM python .\testCLI.py start %COM5%

REM python .\testCLI.py load %COM6% %XML%
REM python .\testCLI.py whitelist-set %COM6% %NODE0% %DUMP%
REM python .\testCLI.py start %COM6%
