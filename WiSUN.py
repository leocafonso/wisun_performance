import serial
import untangle

class WiSUN:
    def __init__(self, COM):
        self.ser = serial.Serial(COM,500000, timeout=3)

    def __del__(self):
        self.ser.close()

    def STATUS_GETR(self,handle):
        self.ser.write('STATUS_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if ret[0] == 'STATUS_GETC':
                return ret[3:]
            else:
                return 'STATUS_GETC 01 01 00 0 0'
        except:
            self.ser.close()

    def DEVCONF_SETR(self,handle):
        self.ser.write('DEVCONF_SETR {} 00 {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                                , format(int(self.DeviceType), '02x')
                                , self.PanID
                                , format(int(self.PanSize), '04x')
                                , '01'                                  
                                , bytes(self.NetworkName, 'ascii').hex()
                                , format(int(self.SixLowPANMTU), '04x')
                                , '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret

        except:
            self.ser.close()

    def DEVCONF_GETR(self,handle):
        self.ser.write('DEVCONF_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def PHYFAN_SETR(self,handle):
        self.ser.write('PHYFAN_SETR {} 00 {} {} {} {} {}'.format(format(int(handle), '02x')
                                , format(int(self.PHYRegulatoryDomain, 16), '02x')
                                , format(int(self.PHYOperatingClass, 16), '02x')
                                , format(int(self.PHYOperatingMode, 16), '02x')                              
                                , format(int(self.PHYTXPower, 16), '02x')
                                , format(int(self.ARIB_STD_T108, 16), '02x')).encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()

    def PHYFAN_GETR(self,handle):
        self.ser.write('PHYFAN_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def UCSCH_SETR(self,handle):
        if (int(self.Excluded_Channel_Control) == 0):
            self.ser.write('UCSCH_SETR {} 00 {} {} {} {} {} {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                                ,format(int(self.Dwell_Interval), '02x')        
                                ,format(int(self.Clock_Drift), '02x')
                                ,format(int(self.Timing_Accuracy), '02x')
                                ,format(int(self.Channel_Plan), '02x')
                                ,format(int(self.Channel_Function), '02x')
                                ,format(int(self.Excluded_Channel_Control), '02x')
                                ,format(int(self.Regulatory_Domain), '02x')
                                ,format(int(self.Operating_Class), '02x')
                                ,format(int(self.CH0), '08x')
                                ,format(int(self.Channel_Spacing), '02x')
                                ,format(int(self.No_Of_Channels), '02x')
                                ,format(int(self.Fixed_Channel), '02x')).encode('ascii'))
        elif (int(self.Excluded_Channel_Control) == 1):
            self.ser.write('UCSCH_SETR {} 00 {} {} {} {} {} {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                                ,format(int(self.Dwell_Interval), '02x')        
                                ,format(int(self.Clock_Drift), '02x')
                                ,format(int(self.Timing_Accuracy), '02x')
                                ,format(int(self.Channel_Plan), '02x')
                                ,format(int(self.Channel_Function), '02x')
                                ,format(int(self.Excluded_Channel_Control), '02x')
                                ,format(int(self.Regulatory_Domain), '02x')
                                ,format(int(self.Operating_Class), '02x')
                                ,format(int(self.CH0), '08x')
                                ,format(int(self.Channel_Spacing), '02x')
                                ,format(int(self.No_Of_Channels), '02x')
                                ,format(int(self.Fixed_Channel), '02x')).encode('ascii'))        
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()

    def UCSCH_GETR(self,handle):
        self.ser.write('UCSCH_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def BCSCH_SETR(self,handle):
        if (int(self.Excluded_Channel_Control) == 0):
            self.ser.write('BCSCH_SETR {} 00 {} {} {} {} {} {} {} {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                                ,format(int(self.Broadcast_Interval), '08x')        
                                ,format(int(self.Broadcast_Schedule_Identifier), '04x')
                                ,format(int(self.Broadcast_Dwell_Interval), '02x')        
                                ,format(int(self.Broadcast_Clock_Drift), '02x')
                                ,format(int(self.Broadcast_Timing_Accuracy), '02x')
                                ,format(int(self.Broadcast_Channel_Plan), '02x')
                                ,format(int(self.Broadcast_Channel_Function), '02x')
                                ,format(int(self.Broadcast_Excluded_Channel_Control), '02x')
                                ,format(int(self.Broadcast_Regulatory_Domain), '02x')
                                ,format(int(self.Broadcast_Operating_Class), '02x')
                                ,format(int(self.Broadcast_CH0), '08x')
                                ,format(int(self.Broadcast_Channel_Spacing), '02x')
                                ,format(int(self.Broadcast_No_Of_Channels), '02x')
                                ,format(int(self.Broadcast_Fixed_Channel), '02x')).encode('ascii'))
        elif (int(self.Excluded_Channel_Control) == 1):
            self.ser.write('BCSCH_SETR {} 00 {} {} {} {} {} {} {} {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                                ,format(int(self.Broadcast_Interval), '08x')        
                                ,format(int(self.Broadcast_Schedule_Identifier), '04x')
                                ,format(int(self.Broadcast_Dwell_Interval), '02x')        
                                ,format(int(self.Broadcast_Clock_Drift), '02x')
                                ,format(int(self.Broadcast_Timing_Accuracy), '02x')
                                ,format(int(self.Broadcast_Channel_Plan), '02x')
                                ,format(int(self.Broadcast_Channel_Function), '02x')
                                ,format(int(self.Broadcast_Excluded_Channel_Control), '02x')
                                ,format(int(self.Broadcast_Regulatory_Domain), '02x')
                                ,format(int(self.Broadcast_Operating_Class), '02x')
                                ,format(int(self.Broadcast_CH0), '08x')
                                ,format(int(self.Broadcast_Channel_Spacing), '02x')
                                ,format(int(self.Broadcast_No_Of_Channels), '02x')
                                ,format(int(self.Broadcast_Fixed_Channel), '02x')).encode('ascii'))      
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()

    def BCSCH_GETR(self,handle):
        self.ser.write('BCSCH_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def STARTR(self,handle):
        self.ser.write('STARTR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def RSTR(self,handle):
        self.ser.write('RSTR {} {}'.format(format(int(handle), '02x'), '0000').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def ROUTELST_GETR(self,handle):
        self.ser.write('ROUTELST_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()

    def NDCACHE_GETR(self,handle):
        self.ser.write('NDCACHE_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()

    def ICMPSR(self,handle, dts ,src, payload, size, mode):
        self.ser.write('ICMPSR {} {} {} {} {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                            ,dts        
                            ,src
                            ,format(int(255), '02x')
                            ,format(int(128), '02x')
                            ,format(int(0), '04x')
                            ,format(int(0), '04x')
                            ,format(int(mode), '02x')
                            ,format(int(size), '04x')
                            ,payload
                            ,format(int(0), '04x')).encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()

    def UDPSR(self,handle, dts ,src,port_dts,port_src, exchange_mode, payload, size, option):
        self.ser.write('UDPSR {} {} {} {} {} {} {} {} {}'.format(format(int(handle), '02x')
                            ,dts        
                            ,src
                            ,port_dts
                            ,port_src
                            ,format(int(exchange_mode), '02x')
                            ,format(int(size), '04x')
                            ,payload
                            ,format(int(option), '04x')).encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()
            

    def WHTLST_SETR(self,handle):
        self.ser.write('WHTLST_SETR {} {} {}'.format(format(int(handle), '02x')
                            ,'00'        
                            ,self.White_EUI64).encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            return ret
        except:
            self.ser.close()
    
    def WHTLST_GETR(self,handle):
        self.ser.write('WHTLST_GETR {} {}'.format(format(int(handle), '02x'), '00').encode('ascii'))
        self.ser.write(b'\x0a')
        try:
            line = self.ser.readline()
            ret = line.decode('ascii').rstrip().split(' ')
            if int(ret[2]) == 0:
                return ret[3:]
        except:
            self.ser.close()

    def load(self, xml):

        obj = untangle.parse(xml)
        self.DeviceType = obj.WiSUNFAN.DeviceConfig.DeviceType.cdata
        self.PanID = obj.WiSUNFAN.DeviceConfig.PanID.cdata
        self.PanSize = obj.WiSUNFAN.DeviceConfig.PanSize.cdata
        self.NetworkName = obj.WiSUNFAN.DeviceConfig.NetworkName.cdata
        self.SixLowPANMTU = obj.WiSUNFAN.DeviceConfig.SixLowPANMTU.cdata

        self.PHYRegulatoryDomain = obj.WiSUNFAN.PHYConfig.PHYRegulatoryDomain.cdata
        self.PHYOperatingMode = obj.WiSUNFAN.PHYConfig.PHYOperatingMode.cdata
        self.PHYOperatingClass = obj.WiSUNFAN.PHYConfig.PHYOperatingClass.cdata
        self.PHYTXPower = obj.WiSUNFAN.PHYConfig.PHYTXPower.cdata
        self.ARIB_STD_T108 = obj.WiSUNFAN.PHYConfig.ARIB_STD_T108.cdata

        self.Dwell_Interval             = obj.WiSUNFAN.UnicastSchedule.Dwell_Interval.cdata
        self.Clock_Drift                = obj.WiSUNFAN.UnicastSchedule.Clock_Drift.cdata
        self.Timing_Accuracy            = obj.WiSUNFAN.UnicastSchedule.Timing_Accuracy.cdata
        self.Channel_Plan               = obj.WiSUNFAN.UnicastSchedule.Channel_Plan.cdata
        self.Channel_Function           = obj.WiSUNFAN.UnicastSchedule.Channel_Function.cdata
        self.Excluded_Channel_Control   = obj.WiSUNFAN.UnicastSchedule.Excluded_Channel_Control.cdata
        self.Regulatory_Domain          = obj.WiSUNFAN.UnicastSchedule.Regulatory_Domain.cdata
        self.Operating_Class            = obj.WiSUNFAN.UnicastSchedule.Operating_Class.cdata
        self.CH0                        = obj.WiSUNFAN.UnicastSchedule.CH0.cdata
        self.Channel_Spacing            = obj.WiSUNFAN.UnicastSchedule.Channel_Spacing.cdata
        self.No_Of_Channels             = obj.WiSUNFAN.UnicastSchedule.No_Of_Channels.cdata
        self.Fixed_Channel              = obj.WiSUNFAN.UnicastSchedule.Fixed_Channel.cdata
        self.Channel_Hop_Count          = obj.WiSUNFAN.UnicastSchedule.Channel_Hop_Count.cdata
        self.Channel_Hop_List           = obj.WiSUNFAN.UnicastSchedule.Channel_Hop_List.cdata
        self.No_Of_Excluded_Ranges      = obj.WiSUNFAN.UnicastSchedule.No_Of_Excluded_Ranges.cdata
        self.Excluded_Ranges            = obj.WiSUNFAN.UnicastSchedule.Excluded_Ranges.cdata
        self.Excluded_Channel_Mask      = obj.WiSUNFAN.UnicastSchedule.Excluded_Channel_Mask.cdata

        self.Broadcast_Interval                       = obj.WiSUNFAN.BroadcastSchedule.Broadcast_Interval.cdata
        self.Broadcast_Schedule_Identifier            = obj.WiSUNFAN.BroadcastSchedule.Broadcast_Schedule_Identifier.cdata
        self.Broadcast_Dwell_Interval                 = obj.WiSUNFAN.BroadcastSchedule.Dwell_Interval.cdata
        self.Broadcast_Clock_Drift                    = obj.WiSUNFAN.BroadcastSchedule.Clock_Drift.cdata
        self.Broadcast_Timing_Accuracy                = obj.WiSUNFAN.BroadcastSchedule.Timing_Accuracy.cdata
        self.Broadcast_Channel_Plan                   = obj.WiSUNFAN.BroadcastSchedule.Channel_Plan.cdata
        self.Broadcast_Channel_Function               = obj.WiSUNFAN.BroadcastSchedule.Channel_Function.cdata
        self.Broadcast_Excluded_Channel_Control       = obj.WiSUNFAN.BroadcastSchedule.Excluded_Channel_Control.cdata
        self.Broadcast_Regulatory_Domain              = obj.WiSUNFAN.BroadcastSchedule.Regulatory_Domain.cdata
        self.Broadcast_Operating_Class                = obj.WiSUNFAN.BroadcastSchedule.Operating_Class.cdata
        self.Broadcast_CH0                            = obj.WiSUNFAN.BroadcastSchedule.CH0.cdata
        self.Broadcast_Channel_Spacing                = obj.WiSUNFAN.BroadcastSchedule.Channel_Spacing.cdata
        self.Broadcast_No_Of_Channels                 = obj.WiSUNFAN.BroadcastSchedule.No_Of_Channels.cdata
        self.Broadcast_Fixed_Channel                  = obj.WiSUNFAN.BroadcastSchedule.Fixed_Channel.cdata
        self.Broadcast_Channel_Hop_Count              = obj.WiSUNFAN.BroadcastSchedule.Channel_Hop_Count.cdata
        self.Broadcast_Channel_Hop_List               = obj.WiSUNFAN.BroadcastSchedule.Channel_Hop_List.cdata
        self.Broadcast_No_Of_Excluded_Ranges          = obj.WiSUNFAN.BroadcastSchedule.No_Of_Excluded_Ranges.cdata
        self.Broadcast_Excluded_Ranges                = obj.WiSUNFAN.BroadcastSchedule.Excluded_Ranges.cdata
        self.Broadcast_Excluded_Channel_Mask          = obj.WiSUNFAN.BroadcastSchedule.Excluded_Channel_Mask.cdata

        self.White_EUI64          = obj.WiSUNFAN.WhiteList.White_EUI64.cdata

        print(self.DEVCONF_SETR(0))
        print(self.DEVCONF_GETR(0))
        print(self.PHYFAN_SETR(0))
        print(self.PHYFAN_GETR(0))
        print(self.UCSCH_SETR(0))
        print(self.UCSCH_GETR(0))
        print(self.BCSCH_SETR(0))
        print(self.BCSCH_GETR(0))
        print(self.WHTLST_SETR(0))
        print(self.WHTLST_GETR(0))

    def start(self):
        return self.STARTR(0)

    def reset(self):
        return self.RSTR(0)

    def status(self):
        return self.STATUS_GETR(0)

    def route_list(self):
        return self.ROUTELST_GETR(0)

    def neighbor_cache(self):
        return self.NDCACHE_GETR(0)

    def whitelist_set(self, addr1, addr2):
        return self.WHTLST_SETR(0,addr1, addr2)
    
    def whitelist_get(self):
        return self.WHTLST_GETR(0)

    def udp_send(self,dts,src,port_dts,port_src, exchange_mode, payload, size, option):
        self.ser.flushInput()
        return self.UDPSR(0,dts,src,port_dts,port_src, exchange_mode, payload, size, option)

    def udp_rcvi(self):
        line = self.ser.readline()
        ret = line.decode('ascii').rstrip().split(' ')
        return ret

    def icmp_ping_req(self,dst,src,payload,size,mode):
        self.ser.flushInput()
        ret = self.ICMPSR(0, dst,src,payload,size,mode)
        err = 1
        if len(ret) == 3:
            if ret[2].isnumeric():
                err = int(ret[2])
        return err

    def icmp_ping_reply(self):
        line = self.ser.readline()
        ret = line.decode('ascii').rstrip().split(' ')
        return ret