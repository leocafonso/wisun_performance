set COM=COM18
set XML=..\RouterNode_US_Band_50kbps_Ch0.xml
set WHTLIST1=7490500000000001
set WHTLIST2=7490500000000003

python .\testCLI.py load %COM% %XML%
python .\testCLI.py whitelist-set %COM% %WHTLIST1% %WHTLIST2%
python .\testCLI.py start %COM%
